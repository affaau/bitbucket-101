# Upload new project/repo from local to BitBucket

1. Initialize the directory under source control.

$> git init

2. Add the existing files to the repository.

$> git add .

3. Commit the files.

$> git commit -m "message"

4. Log into Bitbucket.
5. Create a new repository.
6. Locate the Repository setup page.
7. Choose I have an existing project.
8. Follow the directions in the pane for your repository.

$> cd /path/to/my/repo

$> git remote add origin https://your-username@bitbucket.org/ommunedevelopers/repository-name.git

Pushes up the repo and its refs for the first time

$> git push -u origin --all 

Pushes up any tags

$> git push -u origin --tags



# Pull and update back to BitBucket

To clone a copy into local system

$> git clone https://affaau@bitbucket.org/affaau/test.git

To push changes (all files) to Git repository

$> git add --all

## or add individual files manually
$> git add filename1.extension filename2.extension
##
$> git commit -m 'commit message'

$> git push
*If prompted for authentication, enter bitbucket password.

To latest update from Git repository

$> git pull
